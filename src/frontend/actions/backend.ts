export const BACKEND_CHANNEL_CONNECT = 'BACKEND_CHANNEL_CONNECT';
export const BACKEND_CHANNEL_DISCONNECT = 'BACKEND_CHANNEL_DISCONNECT';

export function backendEvent(eventType: string, attrs: any) {
  return { type: `BACKEND_${eventType}`, ...attrs };
}