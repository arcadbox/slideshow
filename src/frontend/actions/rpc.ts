import { Action } from "redux";
import { FindOptions, FindQuery } from "../../common/rpc/find";

export const RPC_REQUEST = 'RPC_REQUEST';
export const RPC_SUCCESS = 'RPC_SUCCESS';
export const RPC_FAILURE = 'RPC_FAILURE';

export interface RPCAction extends Action {
  job: string
  method: string
  params: any
}

export interface RPCSuccessAction extends RPCAction {
  result: any
}

let nextJob = 0;

export function remoteProcedureCall(method: string, params?: any): RPCAction {
  return { type: RPC_REQUEST, job: `${nextJob++}`,  method, params };
}

export const CLEAR_RPC_STATE = 'CLEAR_RPC_STATE';

export interface ClearRPCStateAction extends Action {
  job: string
}

export function clearRPCState(job: string): ClearRPCStateAction {
  return { type: CLEAR_RPC_STATE, job };
}

export function getCurrentUserInfo() {
  return remoteProcedureCall("getCurrentUserInfo");
}

export function get(modelName: string, id: string) {
  return remoteProcedureCall(`get${modelName}`, { [`${modelName.toLowerCase()}Id`]: id });
}

export function save(modelName: string, model: any) {
  return remoteProcedureCall(`save${modelName}`, { [`${modelName.toLowerCase()}`]: model });
}

export function del(modelName: string, id: string) {
  return remoteProcedureCall(`delete${modelName}`, { [`${modelName.toLowerCase()}Id`]: id });
}

export function find(modelName: string, query?: FindQuery, options?: FindOptions) {
  return remoteProcedureCall(`find${modelName}`, { query, options });
}


export function findSlides(query?: FindQuery, options?: FindOptions) {
  return remoteProcedureCall("findSlides", { query, options });
}
