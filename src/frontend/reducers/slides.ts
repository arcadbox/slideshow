import { Action } from "redux";
import { Setting, Settings, SettingType } from "../../common/models/setting";
import { Slide } from "../../common/models/slide";
import { RPCSuccessAction, RPC_SUCCESS } from "../actions/rpc";

export interface SlideState {
    slides: Array<Slide>
    byId: { [id: string]: Slide}
};

const defaultState = {
    slides: [],
    byId: {}
};

export function slideReducer(state = defaultState, action: Action) {
    switch (action.type) {
        case RPC_SUCCESS:
            return handleRPCSuccess(state, action as RPCSuccessAction);
    };

    return state;
}

function handleRPCSuccess(state: SlideState, action: RPCSuccessAction) {
    switch (action.method) {
        case "findSlides":
            return handleFindSlide(state, action);
    }

    return state;
}

function handleFindSlide(state: SlideState, action: RPCSuccessAction) {
    console.log("FIND SLIDE ACTION", action)
    const newState = {
        ...state,
        slides: action.result,
        byId: {...state.byId, }
    };

    (action.result as Slide[]).forEach(s => newState.byId[s.id] = { ...s })
    
    return newState;
}