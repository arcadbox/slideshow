import { Action } from "redux";
import { Setting, Settings, SettingType } from "../../common/models/setting";
import { RPCSuccessAction, RPC_SUCCESS } from "../actions/rpc";

export interface SettingState {
    byId: { [id: string]: Setting }
};

const defaultState = {
    byId: {
        [Settings.BannerTitle]: { id: Settings.BannerTitle, value: "My App", type: SettingType.TEXT },
    },
};

export function settingReducer(state = defaultState, action: Action) {
    switch (action.type) {
        case RPC_SUCCESS:
            return handleRPCSuccess(state, action as RPCSuccessAction);
    };

    return state;
}

function handleRPCSuccess(state: SettingState, action: RPCSuccessAction) {
    switch (action.method) {
        case "findSetting":
            return handleFindSetting(state, action);
        case "saveSetting":
            return handleSaveSetting(state, action);
        case "getSetting":
            return handleGetSetting(state, action);
    }

    return state;
}


function handleGetSetting(state: SettingState, action: RPCSuccessAction) {
    const setting = action.result as Setting;

    if (!setting || !setting.id) return state;

    const newState = {
        ...state,
        byId: {
           ...state.byId,
           [setting.id]: { ...setting },
        },
    };
        
    return newState;
}
function handleFindSetting(state: SettingState, action: RPCSuccessAction) {
    const newState = {
        ...state,
        byId: {
           ...state.byId,
        },
    };
    
    (action.result as Setting[]).forEach(s => newState.byId[s.id] = { ...s });
    
    return newState;
}

function handleSaveSetting(state: SettingState, action: RPCSuccessAction) {
    const setting = action.result as Setting;
    const newState = {
        ...state,
        byId: {
           ...state.byId,
           [setting.id]: { ...setting },
        },
    };
        
    return newState;
}