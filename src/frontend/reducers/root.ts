import { currentUserReducer, CurrentUserState } from "./currentUser";
import { rpcReducer, RPCState } from "./rpc";
import { settingReducer, SettingState } from "./settings";
import { slideReducer, SlideState } from "./slides";

export interface RootState {
  currentUser: CurrentUserState
  rpc: RPCState
  settings: SettingState
  slides: SlideState,
}

export default {
  rpc: rpcReducer,
  currentUser: currentUserReducer,
  settings: settingReducer,
  slides: slideReducer,
};