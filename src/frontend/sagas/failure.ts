import { Action } from "redux";
import { all, takeLatest } from "redux-saga/effects";

export function* failureSaga() {
    yield all([
        takeLatest((action: Action) => (/_FAILURE$/).test(action.type), handleActionFailure),
    ]);
}

function handleActionFailure(action: any) {
    console.error(action.err || action.error);
}