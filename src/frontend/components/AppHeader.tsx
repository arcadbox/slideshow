import React, { Fragment, FunctionComponent, ReactNode } from 'react';
import { Link } from 'react-router-dom';

export interface AppHeaderProps {
  title?: string
  titleLink?: string
  actionButton?: ReactNode
}

export const AppHeader: FunctionComponent<AppHeaderProps> = ({ title, actionButton, titleLink }) => {
  return (
    <Fragment>
      <section className="hero is-normal app-header">
        <div className="hero-body">
          <div className="container">
            <div className="level is-mobile">
              <div className="level-left">
                <h1 className="title level-item"><Link className="has-text-white" to={titleLink ? titleLink : '/'}>{ title }</Link></h1>
              </div>
              <div className="level-right">
                {
                  actionButton
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};