import React, { CSSProperties, Fragment, ReactNode, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { Settings } from '../../../common/models/setting';
import { AdminButton } from '../../components/AdminButton';
import { AppHeader } from '../../components/AppHeader';
import { RootState } from '../../reducers/root';
import { selectIsAdmin } from '../../selectors/currentUser';
import { useTitle } from '../../hooks/useTitle';
import { find, findSlides } from '../../actions/rpc';
import { Loader } from '../../components/Loader';

const styles: { [name: string]: CSSProperties } = {
  loader: {
    marginTop: '10%',
  },
};

const createSettingSelector = (id: string) => {
  return (state: RootState) => state.settings.byId[id];
};

const homePageSelector = createSelector(
  createSettingSelector(Settings.BannerTitle),
  selectIsAdmin,
  (bannerTitle, isAdmin) => {
    return {
      bannerTitle: bannerTitle ? bannerTitle.value : "",
      isAdmin,
    }
  }
);

export function HomePage() {
  const { isAdmin, bannerTitle } = useSelector(homePageSelector);
  const [ index, setIndex ] = useState(0);
  const timeoutRef = React.useRef();

  const dispatch = useDispatch();

  const { slide } = useSelector((state: RootState) => {
    return {
      slide: state.slides.slides[0]
    };
  })

  useTitle(bannerTitle);

  const buttons: JSX.Element[] = [];
  
  if (isAdmin) {
    buttons.push(<AdminButton key="admin-link" to="/admin" />);
  }

  const resetTimeout = () => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }

  useEffect(() => {
    dispatch(find("Slides"));
  }, []);

  useEffect(() => {
    resetTimeout();
    console.log(index);
    (timeoutRef.current as any) = setTimeout(
      () =>
        setIndex((prevIndex) =>
          prevIndex === slide.items?.length - 1 ? 0 : prevIndex + 1
        ),
      2500
    );

    return () => {
      resetTimeout();
    };
  }, [index])

  if (!slide) {
    return <Loader />
  }

  return (
    <Fragment>
        <div className="slideshow">
          <div
            className="slideshowSlider"
            style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
          >
              {slide.items?.map((s, index) => (
                  <iframe src={s.url} className="slide" frameBorder="0" scrolling="no"></iframe>
              ))}
          </div>
        </div>
    </Fragment>
  );
};

export default HomePage;