import React, { Fragment, FunctionComponent } from 'react';
import { useRouteMatch } from 'react-router';
import { AppHeader } from '../../components/AppHeader';
import { HomeButton } from '../../components/HomeButton';
import { Menu } from './Menu';

export interface AdminPageProps {
    
}

export const AdminPage: FunctionComponent<AdminPageProps> = ({ children }) => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <AppHeader title="Administration"
                titleLink="/admin"
                actionButton={<HomeButton />} />
            <section className="section">
                <div className="container">
                    <div className="columns">
                        <div className="column is-2">
                            <Menu selectedPage={path} />
                        </div>
                        <div className="column is-10">
                            {children}
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    );
};
