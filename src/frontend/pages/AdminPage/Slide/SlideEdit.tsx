import React, { FunctionComponent, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useRouteMatch } from "react-router";
import { Slide } from "../../../../common/models/slide";
import { del, find } from "../../../actions/rpc";
import Loader from "../../../components/Loader";
import { RootState } from "../../../reducers/root";
import { AdminPage } from "../AdminPage";
import { SlideForm } from "./SlideForm";

export interface SlideEditProps {

}

export const SlideEdit:FunctionComponent<SlideEditProps> = () => {
    const dispatch = useDispatch();
    const { params } = useRouteMatch<any>();
    const history = useHistory();
    
    const slide = useSelector<RootState>(state => state.slides.byId[params.slideId]) as Slide;
    console.log(slide);
    if (!slide) {
        console.log("NO SLIDE");
        return <Loader />;
    }

    useEffect(() => {
        dispatch(find("Slides", {eq: {id: params.slideId}}));
    }, []);

    const onDeleteClick = () => {
        dispatch(del("Slide", params.slideId));
        history.push("/admin/slides");
    };

    return (
        <AdminPage>
            <div className="level">
                <div className="level-left">
                </div>
                <div className="level-right">
                    <div className="buttons is-right level-item">
                        <div className="button is-danger" onClick={onDeleteClick}>Supprimer</div>
                    </div>
                </div>
            </div>
            <SlideForm slide={slide}/>
        </AdminPage>
    )
}

export default SlideEdit;