import React, { ChangeEvent, FunctionComponent, MouseEventHandler, useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Slide, SlideItem } from "../../../../common/models/slide";
import { save } from "../../../actions/rpc";

export interface SlideFormProps {
    slide?: Slide
}

export interface SlideFormState {
    slide: Slide
    availableItems: Array<SlideItem>
}

export const SlideForm:FunctionComponent<SlideFormProps> = ({slide}) => {
    const defaultSlide = slide || {
        id: "",
        title: "",
        items: [],
        createdAt: "",
        updatedAt: ""
    };

    const [ state, setState ] = useState<SlideFormState>({
        slide: defaultSlide,
        availableItems: [
            {
                name: "item 1",
                url: "http://......."
            },
            {
                name: "item 2",
                url: "http://......."
            },
            {
                name: "item 3",
                url: "http://......."
            },
            {
                name:"elden ring 1",
                url: "https://d3isma7snj3lcx.cloudfront.net/optim/images/news/30/3050839841/non-seulement-elden-ring-se-remontre-mais-il-a-une-date-de-sortie-5a6e0b7f__1920_1080__180-420-1110-720.jpg"
            },
            {
                name:" elden ring 2",
                url: "https://www.warlegend.net/wp-content/uploads/ELDENRING_16_4K-2048x1152.jpg"
            }
        ]
    });

    const dispatch = useDispatch();
    const history = useHistory();

    const setFieldValue = useCallback((fieldName: string, value: any) => {
        setState(state => ({ ...state, slide: { ...state.slide, [fieldName]: value }}));
    }, [setState]);

    const addItemsValue = useCallback((item: SlideItem) => {
        setState(state => {
            const aItems = [ ...state.availableItems ];
            const iIndex = aItems.indexOf(item);
            aItems.splice(iIndex, 1);
            return {
                ...state, 
                slide: { 
                    ...state.slide, 
                    items: [ ...state.slide.items, { ...item } ]
                },
                availableItems: aItems
            }
        })
    }, [setState]);
    
    const removeItemsValue = useCallback((item: SlideItem) => {
        setState(state => {
            const sItems = [ ...state.slide.items ];
            const iIndex = sItems.indexOf(item);
            sItems.splice(iIndex, 1);
            return {
                ...state, 
                slide: { 
                    ...state.slide, 
                    items: sItems
                },
                availableItems: [ ...state.availableItems, { ...item } ]
            }
        })
    }, [setState]);

    const setSlideTextField = useCallback((fieldName: string, evt:ChangeEvent<HTMLInputElement>) => {
        const value = evt.target.value;
        setFieldValue(fieldName, value);
    }, [setState]);
   
    const onSaveClick = () => {
        dispatch(save("Slide", state.slide));
        history.push("/admin/slides");
    };

    const addItem = useCallback((item: SlideItem, evt: React.MouseEvent<HTMLButtonElement>) => {
        addItemsValue(item);
    }, [setState]);
    
    const removeItem = useCallback((item: SlideItem, evt: React.MouseEvent<HTMLButtonElement>) => {
        removeItemsValue(item);
    }, [setState]);

    return (
        <div className="form">
            <h4 className="is-size-4 mb-5">{ slide ? `Diaporama: ${slide.title}` : "Nouveau diaporama" }</h4>
            <div className="columns">
                <div className="column is-12">
                    <div className="field">
                        <label className="label">Titre du diaporama</label>
                        <div className="control">
                            <input className="input" 
                                value={state.slide.title}
                                onChange={setSlideTextField.bind(null, "title")}
                                type="text"
                                placeholder="Titre du diaporama" />
                        </div>
                    </div>
                </div>
            </div>
            <div className="columns">
                <div className="column is-12">
                    <div className="field">
                        <div className="columns">
                            <div className="column is-6">
                                <table className="table is-fullwidth">
                                    <thead>
                                        <tr>
                                            <th className="is-10">Items disponibles</th>
                                            <th className="has-text-right"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    { state.availableItems?.map((si,k) => {
                                        return (
                                            <tr key={k}>
                                                <td>{si.name}</td>
                                                <td className="has-text-right">
                                                    <button className="button is-small is-block is-pulled-right" onClick={addItem.bind(null, si)}>
                                                        Ajouter
                                                    </button>
                                                </td>
                                            </tr>        
                                        );
                                    })}
                                    </tbody>
                                </table>
                            </div>
                            <div className="column is-6">
                                <table className="table is-fullwidth">
                                    <thead>
                                        <tr>
                                            <th className="is-10">Items sélectionnés</th>
                                            <th className="has-text-right"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    { state.slide.items?.map((si,k) => {
                                        return (
                                            <tr key={k}>
                                                <td>{si.name}</td>
                                                <td className="has-text-right">
                                                    <button className="button is-small is-block is-pulled-right" onClick={removeItem.bind(null, si)}>
                                                        Retirer
                                                    </button>
                                                </td>
                                            </tr>        
                                        );
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="buttons is-right">
                <div className="button is-success" onClick={onSaveClick}>Enregistrer</div>
            </div>
        </div>

    );
}