import React, { FunctionComponent, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { findSlides } from "../../../../backend/slides";
import { find } from "../../../actions/rpc";
import { RootState } from "../../../reducers/root";
import { formatDate } from "../../../util/date";
import { AdminPage } from "../AdminPage";
import { SlideForm } from "./SlideForm";

export interface SlideNewProps {

}

export const SlideNew:FunctionComponent<SlideNewProps> = () => {
    const dispatch = useDispatch();
    
    useEffect(() => {
    
    }, []);

    return (
        <AdminPage>
            <SlideForm />
        </AdminPage>
    )
}

export default SlideNew;