import React, { FunctionComponent, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { findSlides } from "../../../../backend/slides";
import { find } from "../../../actions/rpc";
import { RootState } from "../../../reducers/root";
import { formatDate } from "../../../util/date";
import { AdminPage } from "../AdminPage";

export interface SlideListProps {

}

export const SlideList:FunctionComponent<SlideListProps> = () => {
    const dispatch = useDispatch();
    
    const { slides } = useSelector((state: RootState) => {
        return {
            slides: state.slides.slides,
        };
    });

    useEffect(() => {
        dispatch(find("Slides"))
    }, []);

    return (
        <AdminPage>
            <div className="level">
                <div className="level-left">
                    <h4 className="is-size-4 level-item">Liste des diaporamas ({ slides.length })</h4>
                </div>
                <div className="level-right">
                    <div className="buttons level-item is-right">
                        <Link to="/admin/slides/new" className="button is-primary">Nouveau diaporama</Link>
                    </div>
                </div>
            </div>
            <div className="table-container">
                <table className="table is-striped is-fullwidth">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Date de création</th>
                            <th>Dernière modification</th>
                        </tr>
                    </thead>
                    <tbody>
                        {slides.map(s => {
                            return (
                                <tr key={s.id}>
                                    <td><Link to={`/admin/slides/${s.id}`}>{s.title || '??'}</Link></td>
                                    <td>{s.createdAt ? formatDate(s.createdAt) : '??'}</td>
                                    <td>{s.updatedAt ? formatDate(s.updatedAt) : '??'}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>

        </AdminPage>
    )
}

export default SlideList;