import React, { FunctionComponent, ReactNode, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../reducers/root";
import { AdminPage } from "../AdminPage";
import { find, save } from '../../../actions/rpc';
import { AvailableSettings, SettingDefinition, SettingType } from "../../../../common/models/setting";
import { TextSetting } from "./TextSetting";
import { createSelector } from "reselect";
import { ListSetting } from "./ListSetting";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave } from "@fortawesome/free-solid-svg-icons";

const settingFactory = {
    [SettingType.TEXT]: (d, v, c) => {
        return <TextSetting key={d.id} def={d} onChange={c} value={v} />;
    },
    [SettingType.LIST]: (d, v, c) => {
        return <ListSetting key={d.id} def={d} onChange={c} value={v} />;
    },
} as { [key in SettingType]: (def: SettingDefinition, value: any, onChange: (value: any) => void) => ReactNode }

export interface SettingListProps {
    
};

const selectSettings = (state: RootState) => Object.values(state.settings.byId);

const settingListSelector = createSelector(
    selectSettings,
    (settings) => ({ settings })
);

export const SettingList:FunctionComponent<SettingListProps> = () => {
    const dispatch = useDispatch();
    const [ state, setState ] = useState({
        settings: {},
    });
    
    const store = useSelector(settingListSelector);

    useEffect(() => {
        setState(state => {
            const newSettings = { ...state.settings };
            store.settings.forEach(s => newSettings[s.id] = s.value);
            return { ...state, settings: newSettings };
        });
    }, [store.settings]);

    const onSettingChange = useCallback((settingKey: string, value: any) => {
        setState(state => ({ ...state, settings: { ...state.settings, [settingKey]: value } }));
    }, [setState]);

    const onSaveClick = useCallback(() => {
        store.settings.forEach(set => {
            const value = state.settings[set.id];
            dispatch(save("Setting", { ...set, value }));
        });
    }, [state.settings, store.settings]);

    // Refresh settings on mount
    useEffect(() => {
        dispatch(find("Setting"))
    }, []);

    return (
        <AdminPage>
            <div className="level">
                <div className="level-left">
                    <h4 className="is-size-4 level-item">Paramètres</h4>
                </div>
                <div className="level-right">
                    <button className="button is-success" onClick={onSaveClick}>
                        <FontAwesomeIcon icon={faSave} className="mr-1" />Enregistrer
                    </button>
                </div>
            </div>
            {
                AvailableSettings.map(def => {
                    const factory = settingFactory[def.type];
                    const value = state.settings[def.id] ? state.settings[def.id] : '';
                    const onChange = onSettingChange.bind(null, def.id);

                    return factory(def, value, onChange);
                })
            }
        </AdminPage>
    );
};

export default SettingList;