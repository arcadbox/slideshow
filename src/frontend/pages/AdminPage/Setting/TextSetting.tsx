import React, { ChangeEvent, FunctionComponent, useCallback, useEffect, useState } from "react";
import { SettingDefinition } from "../../../../common/models/setting";

export interface TextSettingProps {
    value: any
    onChange: (value: any) => void
    def: SettingDefinition
}

export const TextSetting:FunctionComponent<TextSettingProps> = ({ value, onChange, def }) => {
    const [ state, setState ] = useState({ value });

    const onTextChange = useCallback((evt: ChangeEvent<HTMLInputElement>) => {
        const value = evt.target.value;
        setState(state => ({ ...state, value }));
    }, [setState]);

    useEffect(() => {
        onChange(state.value);
    }, [state.value]);


    useEffect(() => {
        setState(state => ({ ...state, value }));
    }, [value]);

    return (
        <div className="field">
            <label className="label">{def.label}</label>
            <div className="control">
                <input className="input" type="text" onChange={onTextChange} value={state.value} />
                <p className="help">{def.description}</p>
            </div>
        </div>
    );
}