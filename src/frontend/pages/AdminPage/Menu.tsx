import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";

export interface MenuProps {
    selectedPage?: string
};

export const Menu:FunctionComponent<MenuProps> = ({ selectedPage }) => {
    return (
        <aside className="menu">
            <p className="menu-label">
                Administration
            </p>
            <ul className="menu-list">
                <li>
                    <Link to="/admin/settings"
                        className={`${selectedPage === '/admin/settings' ? 'is-active' : ''}`}>
                        Paramètres
                    </Link>
                </li>
            </ul>
            <p className="menu-label">
                Diaporama
            </p>
            <ul className="menu-list">
                <li>
                    <Link to="/admin/slides"
                        className={`${selectedPage === '/admin/slides' ? 'is-active' : ''}`}>
                        Tous les diaporamas
                    </Link>
                </li>
                <li>
                    <Link to="/admin/slides/new"
                        className={`${selectedPage === '/admin/slides/new' ? 'is-active' : ''}`}>
                        Créer un diaporama
                    </Link>
                </li>
            </ul>
        </aside>
    );
};