export enum ExportFormat {
    CSV = 'csv'
};

export interface ImportEvent {
    type: string
    data: any
}