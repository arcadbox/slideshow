import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas/root'
import rootReducer from '../reducers/root';

const sagaMiddleware = createSagaMiddleware()
const enhancers = [
  applyMiddleware(sagaMiddleware),
];

if (process.env.NODE_ENV !== 'production') {
  const logger = require('redux-logger');
  enhancers.push(applyMiddleware(logger.logger));
}

const combinedReducer = combineReducers(rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore(initialState = {}) {
    const store = createStore(
        combinedReducer,
        initialState,
        composeEnhancers(...enhancers)
    )
    sagaMiddleware.run(rootSaga);
    return store;
}
