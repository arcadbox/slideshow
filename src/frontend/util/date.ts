export function asDate(d: string|Date): Date {
  if (typeof d === 'string') return new Date(d);
  return d;
}

const intl = Intl.DateTimeFormat(navigator.language, { 
  weekday: 'long', 
  month: 'short', 
  day: 'numeric',
  hour: 'numeric', minute: 'numeric', second: 'numeric',
});

export function formatDate(d: Date|string): string {
  d = asDate(d);
  return intl.format(d);
}