import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearRPCState, RPCAction } from "../actions/rpc";
import { RootState } from "../reducers/root";
import { JobState } from "../reducers/rpc";

export const useRPC = (actionFactory: () => RPCAction|void, deps?: React.DependencyList | undefined) => {
    const dispatch = useDispatch();

    const [ jobId, setJobId ] = useState<null|string>(null);
    const [ jobState, setJobState ] = useState({
        running: false,
        failed: false,
        success: false,
    });
    
    useEffect(() => {
        const action = actionFactory();
        
        if (!action) {
            setJobId(null);
            return;
        }

        dispatch(action);
        setJobId(action.job);

        return () => {
            if(!jobId) return;
            dispatch(clearRPCState(jobId));
        };
    }, deps);

    const state = useSelector((rootState:RootState) => {
        if (!jobId) return null;
        return rootState.rpc.jobs[jobId]?.state;
    });

    useEffect(() => {
        setJobState({
            running: state === JobState.Running,
            failed: state === JobState.Failed,
            success: state === JobState.Successful,
        });
    }, [state]);

    return jobState;
};