import { Setting } from "../common/models/setting";
import { FindOptions, FindQuery } from "../common/rpc/find";

const SettingsCollection = "settings"

interface GetSettingParams {
    settingId: string
}

export function getSetting(userId: string, params: GetSettingParams) {
    return store.get(SettingsCollection, params.settingId);
}

interface SaveSettingParams {
    setting: Setting
}

export function saveSetting(userId: string, params: SaveSettingParams) {
    const isAdmin = authorization.isAdmin(userId);
    if (!isAdmin) {
        throw new Error("Unauthorized");
    }

    const setting = params.setting;

    if (setting.id) {
        const existingSetting = store.get(SettingsCollection, setting.id);
        if (!existingSetting || (existingSetting && !existingSetting.createdAt)) {
            setting.createdAt = new Date();
        }
    } else {
        setting.createdAt = new Date();
    }

    setting.updatedAt = new Date();

    return store.save(SettingsCollection, setting);
}

interface FindSettingParams {
    query?: FindQuery
    options?: FindOptions
}

export function findSetting(userId: string, params: FindSettingParams) {
    return store.query(SettingsCollection, params.query, params.options);
}
