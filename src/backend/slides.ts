import { Slide } from "../common/models/slide";
import { FindOptions, FindQuery } from "../common/rpc/find";

const SlidesCollection = "slides"

interface GetSlideParams {
    slideId: string
}

export function getSlide(params: GetSlideParams) {
    return store.save(SlidesCollection, params.slideId);
}

interface FindSlidesParams {
    query?: FindQuery
    options?: FindOptions
}

export function findSlides(params: FindSlidesParams) {
    return store.query(SlidesCollection, params.query, params.options);
}

interface SaveSlideParams {
    slide: Slide
}

export function saveSlide(userId: string, params: SaveSlideParams) {
    const isAdmin = authorization.isAdmin(userId);
    if (!isAdmin) {
        throw new Error("Unauthorized");
    }

    const slide = params.slide;

    if (slide.id) {
        const existingSlide = store.get(SlidesCollection, slide.id);
        if (!existingSlide || (existingSlide && !existingSlide.createdAt)) {
            slide.createdAt = new Date();
        }
    } else {
        slide.createdAt = new Date();
    }

    slide.updatedAt = new Date();

    return store.save(SlidesCollection, slide);
}

interface DeleteSlideParams {
    slideId: string
}

export function deleteSlide(userId: string, params: DeleteSlideParams) {
    const isAdmin = authorization.isAdmin(userId);
    if (!isAdmin) {
        throw new Error("Unauthorized");
    }

    return store.delete(SlidesCollection, params.slideId);
}
