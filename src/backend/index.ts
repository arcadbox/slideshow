import * as main from './main';

Object.keys(main).forEach(k => {
  (global as any)[k] = (main as any)[k];
});