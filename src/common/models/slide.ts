export interface Slide {
    id: string
    title: string
    items: Array<SlideItem>
    createdAt: any
    updatedAt: any
}

export interface SlideItem {
    name: string
    url: string
    order?: number
}