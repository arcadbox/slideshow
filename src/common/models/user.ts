export interface User {
    id: string
    nickname: string
    isAdmin: boolean
}