export enum SettingType {
    TEXT,
    LIST,
}

export interface SettingDefinition extends Setting {
    label: string
    description: string
}

export enum Settings {
    BannerTitle = 'bannerTitle',
}

// List of available settings in the application
export const AvailableSettings: SettingDefinition[] = [
    {
        id: Settings.BannerTitle,
        type: SettingType.TEXT,
        label: "Titre principal",
        description: "Titre affiché dans la bannière principale",
        value: "",
    },
];

export interface Setting {
    id: string
    value: any
    type: SettingType
    meta?: { [key: string]: any }
    createdAt?: Date
    updatedAt?: Date
}