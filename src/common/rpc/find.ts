export interface FindQuery {
    [op: string]: any
}

export interface FindOptions {
    skip?: number
    limit?: number
    orderBy?: string
}