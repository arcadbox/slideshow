# Slideshow

Display slides

Un application [Arcad](https://gitlab.com/arcadbox/arcad).

---

## Démarrer à partir des sources

1. Cloner le projet
2. Surveiller les sources et recompiler l'application à la volée:

    ```shell
    make watch
    ```

Modifier le fichier de configuration de votre instance Arcad:

```yaml
# [...]
app:
  searchPatterns:
  - ./apps/*
  - my-app/dist     # Ajouter le chemin vers le sous répertoire `dist` de votre app
# [...]
```

Puis redémarrer Arcad. Vous devriez voir apparaître la tuile de votre application sur la page d'accueil.

## FAQ

### Générer une version prête à distribuer

```shell
make release
```

Votre application prête à être déployée sera disponible dans le répertoire `release/`.