OPENWRT_DEVICE ?= 192.168.1.1
APP_IDENTIFIER ?= arcad.foxdeveloper.slideshow

watch: clean node_modules
	npm run watch

serve: clean node_modules
	npm run serve

dist: clean node_modules
	npm run build
	
node_modules:
	npm install

clean:
	rm -rf dist release

deploy:
	ssh root@$(OPENWRT_DEVICE) rm -rf /mnt/sda1/arcadbox/apps/$(APP_IDENTIFIER)
	scp -r ./dist root@$(OPENWRT_DEVICE):/mnt/sda1/arcadbox/apps/$(APP_IDENTIFIER)
	ssh root@$(OPENWRT_DEVICE) /etc/init.d/arcadbox-server restart

release: dist
	mkdir -p release
	tar -czf release/$(APP_IDENTIFIER).tar.gz ./dist

.PHONY: webpack dist release