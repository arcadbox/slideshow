const webpack = require('webpack')
const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = [
  merge(common[0], {
    devtool: 'inline-source-map',
    mode: 'development',
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new BundleAnalyzerPlugin(),
    ],
    output: {
      path: path.join(__dirname, 'dist/public'),
      publicPath: './',
      filename: '[name].[hash:8].js',
      chunkFilename: '[name].[chunkhash].chunk.js'
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist/public'),
      compress: true,
      port: 9000,
      writeToDisk: true,
      hotOnly: true,
      hot: true,
    }
  }),
  merge(common[1], {
    mode: 'development',
    output: {
      path: path.join(__dirname, 'dist/backend'),
      publicPath: './',
      filename: 'main.js'
    },
  })
]